//
//  MyView.m
//  AutomationTest
//
//  Created by Dave Henson on 5/4/15.


#import "MyView.h"

@implementation MyView

-(instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];

    if (self)
    {
        textToDraw = @"this is a test two";
    }

    return self;
}


- (void)drawRect:(CGRect)rect
{
    [textToDraw drawAtPoint:CGPointMake(10, 10) withAttributes:nil];
}


-(void)dealloc
{
    [super dealloc];
    
    [textToDraw dealloc];
}

@end
