//
//  ViewController.m
//  AutomationTest
//
//  Created by Dave Henson on 5/4/15.

#import "ViewController.h"

@implementation ViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.view.backgroundColor = [UIColor darkGrayColor];
    
    tView = [[UITextView alloc] initWithFrame:CGRectMake(10, 100, 300, 300)];
    
    
    NSString *path = [[NSBundle mainBundle] pathForResource:@"testfile" ofType:@"txt"];
    NSURL *fileURL = [NSURL fileURLWithPath:path];
    
    NSString *result = [NSString stringWithContentsOfURL:fileURL encoding:NSUTF8StringEncoding error:nil];
    
    tView.text = @"test";
    
    if(result)
    {
      tView.text = result;
    }
    
    [self.view addSubview:tView];
    
}






@end
